require('dotenv').config();

// load the things we need
const config = require('./config')
const express = require('express');
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'src/views');

const favicon = require('serve-favicon');

app.use(favicon('favicon.ico'));

app.use(express.static('public'));

const fs = require('fs');
app.get('/*', (req, res) => {
    let localPath = decodeURIComponent(req.url);
    if (localPath === "/")
        localPath = "";

    let absolutePath = config.folder + decodeURIComponent(req.url);

    if (!fs.existsSync(absolutePath))
        absolutePath = config.folder;

    const file = fs.lstatSync(absolutePath)

    if (file.isFile()) {
        res.sendFile(absolutePath);
    } else {
        if (!file.isDirectory())
            absolutePath = config.folder;

        let files = [];
        fs.readdirSync(absolutePath).forEach(
            file => {
                files.push({
                    name: file,
                    link: localPath + "/" + file
                })
            }
        );

        const collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
        files = files.sort((a, b) => collator.compare(a.name, b.name));
        res.render('pages/index', {files});
    }
});

app.listen(config.port, () => {
    console.log(`Server listening on port ${config.port}`);
});
